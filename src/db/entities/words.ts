import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany
} from "typeorm";
import { Score } from './score';

@Entity()
export class Word extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column("varchar", { nullable: false })
  content!: string;

  @Column("boolean", { nullable: false, default: false })
  isCurrent!: boolean;

  @Column("boolean", { nullable: false, default: false })
  wasUsed!: boolean;

  @Column("integer", { nullable: false, default: 0 })
  countMatch!: number;

  @Column("timestamp", { nullable: true, default: null })
  startedAt!: Date;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @OneToMany(() => Score, (score: Score) => score.word)
  public score!: Score[];

}
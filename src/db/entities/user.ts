import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany
} from "typeorm";
import { Score } from './score';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number | undefined;

  @Column("varchar", { nullable: true, default: null})
  name!: string;

  @Column("integer", { nullable: false, default: 0 })
  gamesPlayed!: number;

  @Column("integer", { nullable: false, default: 0 })
  gamesWon!: number;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @OneToMany(() => Score , (score: Score)  => score.user)
  public score!: Score[];

}
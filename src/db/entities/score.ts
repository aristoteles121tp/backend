import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne
} from "typeorm";
import { User } from './user';
import { Word } from './words';

@Entity()
export class Score extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number | undefined;

  @Column("integer", { nullable: false, default: 0 })
  tries!: number;

  @Column("boolean", { nullable: true, default: null })
  isWinner!: boolean;

  @Column("integer", { nullable: true, default: null })
  wordId!: number;

  @Column("integer", { nullable: true, default: null })
  userId!: number;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @ManyToOne(() => Word, (word: Word) => word.score)
  public word!: Word

  @ManyToOne(() => User, (user: User) => user.score)
  public user!: User

}
import { Repository } from "typeorm";
import { Word } from "./../../entities";

const createWord = async (
  params: Omit<Partial<Word>, 'id'>
): Promise<Word> => {
  const wordRepository: Repository<Word> = (global as any).dbSource.getRepository(Word);
  const newWord: Word = await wordRepository.create(params);
  const wordCreated = await wordRepository.save(newWord);
  return wordCreated;
}

const createManyWords = async (
  params: Omit<Partial<Word>, 'id'>[]
): Promise<Word[]> => {
  const wordRepository: Repository<Word> = (global as any).dbSource.getRepository(Word);
  const newWords: Word[] = await wordRepository.create(params)
  const wordsCreated = await wordRepository.save(newWords);
  return wordsCreated;
}

export {
  createWord,
  createManyWords
}
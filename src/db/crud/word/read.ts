import { FindManyOptions, Repository } from "typeorm";
import { Word } from "../../entities";

const getWords = async (
  where?: Omit<Partial<Word>, 'id'>[],
  order?: { [keyof: string]: 'DESC' | 'ASC' },
  select?: (keyof Partial<Word>)[],
  take?: number
): Promise<Word[]> => {
  const wordRepository: Repository<Word> = (global as any).dbSource.getRepository(Word);
  const findOptions: FindManyOptions<Word> = {};
  if(where) {
    findOptions.where = where
  }
  if(order) {
    findOptions.order = order
  }
  if(select) {
    findOptions.select = select;
  }
  if(take) {
    findOptions.take = take;
  }
  const wordsFound: Word[] = await wordRepository.find(findOptions);
  return wordsFound;
}

const getCountWords = async (): Promise<number> => {
  const wordRepository: Repository<Word> = (global as any).dbSource.getRepository(Word);
  const countWords: number = await wordRepository.count();
  return countWords;
}

export {
  getWords,
  getCountWords
}
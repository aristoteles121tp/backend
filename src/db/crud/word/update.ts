import { Repository } from "typeorm";
import { Word } from "../../entities";

const updateWord = async (
  score: Word
): Promise<Word> => {
  const wordRepository: Repository<Word> = (global as any).dbSource.getRepository(Word);
  const wordUpdated: Word = await wordRepository.save(score);
  return wordUpdated;
}

export {
  updateWord
}
import * as createWord from './create';
import * as readWord from './read';
import * as updateWord from './update';

const wordCrud = {
  ...createWord,
  ...readWord,
  ...updateWord
}

export {
  wordCrud
};

import * as createUser from './create';
import * as readUser from './read';
import * as updateUser from './update';


const userCrud = {
  ...createUser,
  ...readUser,
  ...updateUser
}

export {
  userCrud
};

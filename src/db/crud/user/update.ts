import { Repository } from "typeorm";
import { User } from "../../entities";

const updateUser = async (
  score: User
): Promise<User> => {
  const scoreRepository: Repository<User> = (global as any).dbSource.getRepository(User);
  const scoreUpdated: User = await scoreRepository.save(score);
  return scoreUpdated;
}

export {
  updateUser
}
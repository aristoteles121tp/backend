import { FindManyOptions, Repository } from "typeorm";
import { User } from "../../entities";

const getUser = async (
  userId: number | string
): Promise<User | undefined> => {
  const userRepository: Repository<User> = (global as any).dbSource.getRepository(User);
  const userFound: User | undefined = await userRepository.findOne(userId);
  return userFound;
}

const getManyUser = async (
  where?: Omit<Partial<User>, 'id'>[] | null,
  order?: { [keyof: string]: 'DESC' | 'ASC' } | null,
  select?: (keyof Partial<User>)[] | null,
  take?: number | null
): Promise<User[]> => {
  const userRepository: Repository<User> = (global as any).dbSource.getRepository(User);
  const findOptions: FindManyOptions<User> = {};
  if(where) {
    findOptions.where = where
  }
  if(order) {
    findOptions.order = order
  }
  if(select) {
    findOptions.select = select;
  }
  if(take) {
    findOptions.take = take;
  }
  const userFound: User[] = await userRepository.find(findOptions);
  return userFound;
}

export {
  getUser,
  getManyUser
}
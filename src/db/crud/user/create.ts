import { Repository } from "typeorm";
import { User } from "./../../entities";

const createUser = async (
  params: Omit<Partial<User>, 'id'>
): Promise<User> => {
  const userRepository: Repository<User> = (global as any).dbSource.getRepository(User);
  const newUser: User = await userRepository.create(params);
  const userCreated = await userRepository.save(newUser);
  return userCreated;
}

export {
  createUser
}
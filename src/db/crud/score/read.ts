import { FindManyOptions, Repository } from "typeorm";
import { Score } from "../../entities";

const getScores = async (
  where: Omit<Partial<Score>, 'id'>[],
  order: { [keyof: string]: 'DESC' | 'ASC' } = { updatedAt: 'DESC' },
  take: number = 10,
  relations?: string[]
): Promise<Score[]> => {
  const scoreRepository: Repository<Score> = (global as any).dbSource.getRepository(Score);
  const findOptions: FindManyOptions<Score> = {
    where,
    order
  };
  if(take) {
    findOptions.take = take;
  }
  if(relations?.length) {
    findOptions.relations = relations;
  }
  const scoresFound: Score[] = await scoreRepository.find(findOptions);
  return scoresFound;
}

export {
  getScores
}
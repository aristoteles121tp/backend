import { Repository } from "typeorm";
import { Score } from "../../entities";

const updateScore = async (
  score: Score
): Promise<Score> => {
  const scoreRepository: Repository<Score> = (global as any).dbSource.getRepository(Score);
  const scoreUpdated: Score = await scoreRepository.save(score);
  return scoreUpdated;
}

export {
  updateScore
}
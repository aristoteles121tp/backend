import { Repository } from "typeorm";
import { Score } from "./../../entities";

const createScore = async (
  params: Omit<Partial<Score>, 'id'>
): Promise<Score> => {
  const scoreRepository: Repository<Score> = (global as any).dbSource.getRepository(Score);
  const newScore: Score = await scoreRepository.create(params);
  const scoreCreated = await scoreRepository.save(newScore);
  return scoreCreated;
}

export {
  createScore
}
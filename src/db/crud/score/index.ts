import * as createScore from './create';
import * as readScore from './read';
import * as updateScore from './update';


const scoreCrud = {
  ...createScore,
  ...readScore,
  ...updateScore
}

export {
  scoreCrud
};

import "reflect-metadata";
import { Connection, createConnection } from "typeorm";
import { User, Word, Score } from "./entities";
import { dbConfig } from './../config/db-config';

const initializeDB = async (): Promise<Connection> => {
  try {
    const dbSource: Connection = await createConnection({
      username: dbConfig.username,
      password: dbConfig.password,
      database: dbConfig.database,
      host: dbConfig.host,
      port: dbConfig.port,
      type: dbConfig.type as any,
      synchronize: dbConfig.synchronize,
      logging: dbConfig.logging,
      dropSchema: dbConfig.dropSchema,
      migrationsRun: dbConfig.migrationsRun,
      entities: [User, Word, Score]
    });
    console.log("Data base has been initialized!");
    return dbSource;
  } catch (error) {
    console.error("Error during Data base initialization:", error);
    return error as any;
  }
}

export {
  initializeDB
};

require('dotenv').config()
import { Connection } from 'typeorm';
import { app } from './app';
import { initializeDB } from './db';
import { loadWordsInDb } from './backend/services/load-words-in-db';
import { updateCurrentWord } from './backend/schedules';
import { scheduleJob } from 'node-schedule';
import { schedule } from './backend/constants';

const port = process.env.PORT || 3000;

async function executeTasksAfterStartServer (): Promise<void> {
  const dbSource: Connection = await initializeDB();
  (global as any).dbSource = dbSource;
  await loadWordsInDb();
  return;
}

function runSchedules(): void {
  const currentWordJob = scheduleJob(schedule.rule, updateCurrentWord);
  return;
}

app.listen(port, async () => {
  await executeTasksAfterStartServer();
  await updateCurrentWord();
  runSchedules();
  console.info(`Server runing in port: ${port}`);
});

import { inspect } from 'util';
import axios from 'axios';
import axiosRetry from 'axios-retry';
import { HttpStatus, HttpMethods, axiosConfig } from './../constants';
import qs from 'qs';

class FetchRequest {
  readonly timeout = axiosConfig.requests.timeout;
  private readonly logMsg = '[FetchRequest]';

  constructor(
    enableRetry = false,
    retries: number = axiosConfig.requests.retries
  ) {
    if (enableRetry) {
      this.enableRetry(retries);
    }
  }

  private createQueryStringFromObject(params: { [keyof: string]: any }): string {
    const queryString = qs.stringify(params, { encodeValuesOnly: true });
    return `?${queryString}`;
  }

  async sendRequest(
    url: string,
    path: string | null,
    method: HttpMethods,
    headers: any = {},
    body: any = {},
    query: { [keyof: string]: any } | null = null,
    printResult: boolean = true
  ): Promise<any> {
    let route = '';
    if (path) {
      route = `${url}/${path}${
        query ? this.createQueryStringFromObject(query) : ''
      }`;
    } else {
      route = `${url}${query ? this.createQueryStringFromObject(query) : ''}`;
    }

    const axiosConfig: any = {
      method,
      url: route,
      headers,
      timeout: this.timeout,
    };

    if (method === HttpMethods.GET) {
      Object.assign(axiosConfig, { params: body });
    } else {
      Object.assign(axiosConfig, { data: body });
    }

    console.info(
      `${this.logMsg} Requesting:`,
      inspect(
        {
          route,
          axiosConfig,
        },
        { depth: 5 }
      )
    );
    try {
      const result = await axios(axiosConfig);

      if (printResult) {
        console.info(
          `-------------------------------${this.logMsg}----------------------------------------`
        );
        console.info(
          `${this.logMsg} Request result:`,
          inspect(
            {
              route,
              status: result.status,
              data: result.data,
            },
            { depth: 5 }
          )
        );
        console.info(
          `-------------------------------${this.logMsg}----------------------------------------`
        );
      }
      if (result.status < HttpStatus.badRequest) {
        return Promise.resolve(result.data);
      }
      return Promise.reject(result.data);
    } catch (error) {
      console.error(
        `${this.logMsg} Error requesting ${route}`,
        inspect(error, { depth: 5 })
      );
      throw error;
    }
  }

  private retryCondition(error: any) {
    return !error.response || error.response.status !== HttpStatus.ok;
  }

  private enableRetry(retries: number) {
    axiosRetry(axios, {
      retries,
      retryDelay: axiosRetry.exponentialDelay,
      retryCondition: this.retryCondition,
    });
  }
}

export { FetchRequest };

enum HttpMethods {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE'
}

enum HttpStatus {
  ok = 200,
  created = 201,
  badRequest = 400,
  unauthorized = 401,
  notFound = 404,
  internalServerError = 500
}

const axiosConfig = {
  requests: {
    timeout: 30000,
    retries: 3
  }
};

const urlWords: string = 'https://gitlab.com/d2945/words/-/raw/main/words.txt';

const gameRules = {
  wordLengthMax: parseInt(process.env.GAME_WORD_LENGTH_MAX ?? '5', 10),
  valuesToQualifierLetter: {
    exactMatch: 1,
    match: 2,
    notMatch: 3
  },
  maxTriesAllow: parseInt(process.env.GAME_MAX_TRIES_BYWORD ?? '5', 10),
};

const schedule = {
  rule: '*/1 * * * *'
};

export {
  HttpMethods,
  HttpStatus,
  axiosConfig,
  urlWords,
  gameRules,
  schedule
};

import { FetchRequest } from './../utils';
import { urlWords, HttpMethods, gameRules } from './../constants';
import { wordCrud } from './../../db/crud';
import { Word } from '../../db/entities';

const logMsg: string = '[loadWordsInDb]';

async function loadWordsInDb(): Promise<void> {
  try {
    const fetchRequest: FetchRequest = new FetchRequest();
    const wordsTxt: string = await fetchRequest.sendRequest(
      urlWords,
      null,
      HttpMethods.GET,
      undefined,
      undefined,
      undefined,
      false
    );
    const wordsArray: string [] = wordsTxt.split('\n');
    const paramsToCreateWords: Omit<Partial<Word>, 'id'>[] = [];
    wordsArray.forEach(word => {
      if(word.length === gameRules.wordLengthMax) {
        paramsToCreateWords.push({
          content: word
        });
      }
    });
    if(!paramsToCreateWords.length) { return; }
    console.info(`-------------------------------${logMsg}------------------------------------------`);
    console.info(`${logMsg} Saving words in data base...`);
    const wordsCreated: Word[] = await wordCrud.createManyWords(paramsToCreateWords);
    console.info(`${logMsg} ${wordsCreated.length} words saved in data base...`);
    console.info(`-------------------------------${logMsg}------------------------------------------`);
    return;
  } catch(error) {
    console.error(`${logMsg} Error to load word in db`, error);
    throw error;
  }

};
 
export {
  loadWordsInDb
};

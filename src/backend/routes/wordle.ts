import { Router, Request, Response } from 'express';
import {
  playGameController,
  validateWordController,
  getUserGamesController,
  getBestGamersController,
  wordMoreViewdController,
  getCurrentWordController
} from '../controllers';
import { validateJWT, validateCurrenWord } from '../middlewares';

const router: Router = Router();

router.get('/', async (req: Request, res: Response) => {
  return res.status(200).send('Welcome to Wordle, the best game of words!')
});

// Wordle
router.post('/play', playGameController);
router.post('/validate-word', [validateJWT, validateCurrenWord], validateWordController);

// Users
router.get('/user', [validateJWT], getUserGamesController);
router.get('/user/top', getBestGamersController);

// Word
router.get('/word/top', wordMoreViewdController);
router.get('/word/current', getCurrentWordController);


export { router };

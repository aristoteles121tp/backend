import { User } from '../../../db/entities';
import { Request, Response } from 'express';
import { HttpStatus } from '../../constants';
import { userCrud } from '../../../db/crud';


async function getUserGamesController(req: Request, res: Response) {
  const user: User = (req as any).user;
  return res.status(HttpStatus.ok).json({
    user: {
      id: user.id,
      name: user.name,
      gamesPlayed: user.gamesPlayed,
      gamesWon: user.gamesWon
    }
  });
}

async function getBestGamersController(req: Request, res: Response) {
  try {
    const usersFound: User[] = await userCrud.getManyUser(
      [],
      { gamesWon: 'DESC' },
      ['name', 'gamesWon'],
      10
    );
    return res.status(HttpStatus.ok).json(usersFound);
  } catch (error) {
    console.error('Error to get best gamers:', error);
    return res.status(HttpStatus.internalServerError).json({
      error: {
        message: 'Error to get best gamers.',
        details: (error as any).message
      }
    });
  }
}

export {
  getUserGamesController,
  getBestGamersController
};

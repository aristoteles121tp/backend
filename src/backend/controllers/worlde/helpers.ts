import { JWTUtil } from '../../utils/jwt-utils';
import { User, Word, Score } from '../../../db/entities';
import { userCrud, wordCrud, scoreCrud } from '../../../db/crud';
import { IWordQualification, IExededTRiesOrWin } from '../../interfaces/interface';
import { gameRules } from '../../constants';

const createAccessToken = (
  userId: number,
  userName: string | undefined = undefined
): string => {
  const jwtUtils: JWTUtil = new JWTUtil();
  const payload: any = {
    user: {
      id: userId,
      name: null
    }
  };
  if (typeof userName === 'string') {
    payload.user.name = userName;
  }
  const accessToken = jwtUtils.getTokenBearer(payload);
  return accessToken;
};

const createNewUser = async (
  userName: string | undefined = undefined
): Promise<User> => {
  const paramsToCreateUser: Omit<Partial<User>, 'id'> = {};
  if (typeof userName === 'string') {
    paramsToCreateUser.name = userName;
  }
  const newUser: User = await userCrud.createUser(paramsToCreateUser);
  return newUser;
}

function compareWords(
  wordRecived: string,
  currentWord: string
): IWordQualification {
  const wordQualification: IWordQualification = {
    isWinner: true,
    response: []
  };
  const { exactMatch, match, notMatch } = gameRules.valuesToQualifierLetter;
  for (let i = 0; i < wordRecived.length; i++) {
    const letterRecived = wordRecived[i];
    let value: number = notMatch;
    for (let j = 0; j < currentWord.length; j++) {
      const currentLetter = currentWord[j];
      if (letterRecived === currentLetter) {
        if (i === j) { // letter exact match
          value = exactMatch;
        } else { // letter match
          if (value !== exactMatch) {
            value = match;
          }
        }
      }
    }
    if (value === gameRules.valuesToQualifierLetter.notMatch) {
      wordQualification.isWinner = false;
    }
    wordQualification.response.push({
      letter: letterRecived,
      value
    });
  }
  return wordQualification;
}

async function getCurrentWord(): Promise<Word | null> {
  const conditionsToGetWords: Omit<Partial<Word>, 'id'>[] = [
    {
      isCurrent: true
    }
  ];
  const words: Word[] = await wordCrud.getWords(conditionsToGetWords);
  if (!words?.length) {
    return null;
  }
  return words[0];
}

async function addTrieOfValidateWordToUser(
  user: User,
  wordQualification: IWordQualification,
  currentWord: Word
): Promise<void> {
  const conditionsToGetScores: Omit<Partial<Score>, 'id'>[] = [
    {
      userId: user.id
    }
  ];
  const userScoresFound: Score[] = await scoreCrud.getScores(conditionsToGetScores);
  let scoreUpdated: Score;
  if (userScoresFound.length) {
    const userScore: Score = userScoresFound[0];
    userScore.tries = userScore.tries + 1;
    if (wordQualification.isWinner) {
      userScore.isWinner = true;
      user.gamesWon = user.gamesWon + 1;
      await userCrud.updateUser(user);
      currentWord.countMatch = currentWord.countMatch + 1;
      await wordCrud.updateWord(currentWord);
    }
    scoreUpdated = await scoreCrud.updateScore(userScore);
  } else {
    const paramsTocreateScore: Omit<Partial<Score>, 'id'> = {
      userId: user.id,
      wordId: currentWord.id,
      tries: 1,
      isWinner: wordQualification.isWinner
    };
    scoreUpdated = await scoreCrud.createScore(paramsTocreateScore);
    user.gamesPlayed = user.gamesPlayed + 1;
    if (wordQualification.isWinner) {
      user.gamesWon = user.gamesWon + 1;
      currentWord.countMatch = currentWord.countMatch + 1;
      await wordCrud.updateWord(currentWord);
    }
    await userCrud.updateUser(user);
  }
  return;
}

async function userExceededTriesForWordOrWin(
  user: User,
  currentWord: Word
): Promise<IExededTRiesOrWin> {
  const response: IExededTRiesOrWin = {
    exceededTries: false,
    wasWon: false
  };
  const conditionsToGetScores: Omit<Partial<Score>, 'id'>[] = [
    {
      userId: user.id,
      wordId: currentWord.id
    }
  ];
  const userScoresFound: Score[] = await scoreCrud.getScores(conditionsToGetScores);
  if (userScoresFound.length) {
    const userScore: Score = userScoresFound[0];
    if (userScore.tries >= gameRules.maxTriesAllow) {
      response.exceededTries = true;
    }
    if(userScore.isWinner) {
      response.wasWon = true;
    }
  }
  return response;
}

export const wordleHelpers = {
  createAccessToken,
  createNewUser,
  compareWords,
  getCurrentWord,
  addTrieOfValidateWordToUser,
  userExceededTriesForWordOrWin
}
import { userCrud, wordCrud } from '../../../db/crud';
import { User, Word } from '../../../db/entities';
import { Request, Response } from 'express';
import { HttpStatus } from '../../constants';
import { wordleHelpers } from './helpers';
import { IWordQualification } from '../../interfaces';
import moment from 'moment';

async function playGameController(req: Request, res: Response) {
  const { userName, userId } = req.body as any;
  const response = {
    accessToken: ''
  };
  try {
    if (typeof userId === 'string' || typeof userId === 'number') {
      const userFound: User | undefined = await userCrud.getUser(userId);
      let userIdToCreateToken: number;
      if (userFound?.id) {
        userIdToCreateToken = +userFound.id;
      } else {
        const newUser: User = await wordleHelpers.createNewUser(userName);
        userIdToCreateToken = +newUser.id!;
      }
      response.accessToken = wordleHelpers.createAccessToken(userIdToCreateToken, userName);
    } else {
      const newUser: User = await await wordleHelpers.createNewUser(userName);
      response.accessToken = wordleHelpers.createAccessToken(newUser.id!, userName);
    }
    return res.status(HttpStatus.ok).json(response);
  } catch (error) {
    console.error('Error to get acces to game:', error);
    return res.status(HttpStatus.internalServerError).json({
      error: {
        message: 'Error to get acces to game.',
        details: (error as any).message
      }
    });
  }
}

async function validateWordController(req: Request, res: Response) {
  const wordRecived: string = req.body.word;
  try {
    const currentWord: Word | null = await wordleHelpers.getCurrentWord();
    if (!currentWord) {
      return res.status(HttpStatus.ok).json({
        ok: true,
        message: 'Game is not available.'
      });
    }
    const wordQualification: IWordQualification = wordleHelpers.compareWords(wordRecived, currentWord.content);
    await wordleHelpers.addTrieOfValidateWordToUser((req as any).user, wordQualification, currentWord);
    return res.status(HttpStatus.ok).json(wordQualification);
  } catch (error) {
    console.error('Error to validate word:', error);
    return res.status(HttpStatus.internalServerError).json({
      error: {
        message: 'Error to validate word.',
        details: (error as any).message
      }
    });
  }
}

async function wordMoreViewdController(req: Request, res: Response) {
  try {
    const bestScores: Word[] = await wordCrud.getWords(
      [],
      { countMatch: 'DESC' },
      ['content', 'countMatch'],
      10
    );
    return res.status(HttpStatus.ok).json(bestScores);
  } catch (error) {
    console.error('Error to get word more viewed:', error);
    return res.status(HttpStatus.internalServerError).json({
      error: {
        message: 'Error to get word more viewed.',
        details: (error as any).message
      }
    });
  }
}

async function getCurrentWordController(req: Request, res: Response) {
  try {
    const conditionsToGetCurrentWords: Omit<Partial<Word>, 'id'>[] = [
      {
        isCurrent: true
      }
    ];
    const wordsCurrent: Word[] = await wordCrud.getWords(conditionsToGetCurrentWords);
    if(!wordsCurrent?.length) {
      return res.status(HttpStatus.ok).json({
        ok: true,
        message: 'Game is not available.'
      });
    }
    const currentWord: Word = wordsCurrent[0];
    const currentWordReponse = {
      content: currentWord.content,
      isCurrent: currentWord.isCurrent,
      startedAt: moment(currentWord.startedAt).toLocaleString()
    }
    return res.status(HttpStatus.ok).json(currentWordReponse);
  } catch (error) {
    console.error('Error to get current word:', error);
    return res.status(HttpStatus.internalServerError).json({
      error: {
        message: 'Error to get current word.',
        details: (error as any).message
      }
    });
  }
}

export {
  playGameController,
  validateWordController,
  wordMoreViewdController,
  getCurrentWordController
};

import { Word } from '../../db/entities';
import { wordCrud } from '../../db/crud/word';
import moment from 'moment';

function getRandomNumber(min: number, max: number) {
  return Math.floor(Math.random() * (max - min)) + min;
}

async function updateCurrentWord(): Promise<void> {
  try {
    const conditionsToGetCurrentWords: Omit<Partial<Word>, 'id'>[] = [
      {
        isCurrent: true
      }
    ];
    const conditionsToGetWordsNotUsed: Omit<Partial<Word>, 'id'>[] = [
      {
        wasUsed: false
      }
    ];
    const wordsCurrent: Word[] = await wordCrud.getWords(conditionsToGetCurrentWords);
    const wordsNotUsed: Word[] = await wordCrud.getWords(conditionsToGetWordsNotUsed);
    const countWordsNotUsed: number = wordsNotUsed.length;
    let newCurrentWord: Word | null = null;
    if (wordsCurrent?.length) {
      await Promise.all(wordsCurrent.map(wordI => {
        wordI.isCurrent = false;
        wordI.wasUsed = true;
        return wordCrud.updateWord(wordI);
      }));
    }
    if (countWordsNotUsed === 0) {
      console.warn('Not found wods to wordle.')
    } else if (countWordsNotUsed === 1) {
      newCurrentWord = wordsNotUsed[0];
      newCurrentWord.isCurrent = true;
    } else {
      const index: number = getRandomNumber(0, countWordsNotUsed);
      newCurrentWord = wordsNotUsed[index];
      newCurrentWord.isCurrent = true;
    }
    if (newCurrentWord) {
      newCurrentWord.startedAt = moment().toDate();
      newCurrentWord = await wordCrud.updateWord(newCurrentWord);
    }
    console.info(`New current word is: `, JSON.stringify(newCurrentWord, null, 4));
    return;
  } catch (error) {
    console.error('Error tu upgrade the current word:', error);
  }
}

export {
  updateCurrentWord
};

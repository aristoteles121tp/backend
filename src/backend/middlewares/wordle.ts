import { Request, Response, NextFunction } from "express";
import { HttpStatus } from '../constants';
import { User, Word } from '../../db/entities';
import { wordleHelpers } from '../controllers/worlde/helpers';
import { IExededTRiesOrWin } from '../interfaces/interface';

async function validateCurrenWord(req: Request, res: Response, next: NextFunction) {
  const logMsg = '[validateCurrenWord]';
  try {
    const user: User = (req as any).user;
    const currentWord: Word | null = await wordleHelpers.getCurrentWord();
    if (!currentWord) {
      return res.status(HttpStatus.ok).json({
        ok: true,
        message: 'Game is not available.'
      });
    }
    const exceededTriesOrWin: IExededTRiesOrWin = await wordleHelpers.userExceededTriesForWordOrWin(
      user,
      currentWord
    );
    if(exceededTriesOrWin.exceededTries) {
      return res.status(HttpStatus.ok).json({
        message: 'User was exceeded tries to current word.'
      });
    }
    if(exceededTriesOrWin.wasWon) {
      return res.status(HttpStatus.ok).json({
        message: 'User was won with current word.'
      });
    }
    return next();
  } catch (error) {
    console.error(`${logMsg} Error to validateCurrenWord`, error);
    return res.status(HttpStatus.unauthorized).json({
      error: {
        message: 'Error to validate validateCurrenWord.',
        details: (error as any).message
      }
    });
  }
}

export {
  validateCurrenWord
};

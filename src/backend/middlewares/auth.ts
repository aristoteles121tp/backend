import { Request, Response, NextFunction } from "express";
import { JWTUtil } from '../utils/jwt-utils';
import { HttpStatus } from '../constants';
import { userCrud } from '../../db/crud';
import { User } from '../../db/entities';

async function validateJWT(req: Request, res: Response, next: NextFunction) {
  const logMsg = '[validateJWT]';
  try {
    const authorization: string = req.headers.authorization as string;
    const accessToken: string = authorization.split(' ')[1];
    const jwtUtils: JWTUtil = new JWTUtil();
    const payload = jwtUtils.verifyJWTAndGetData(accessToken);
    console.info(`${logMsg} Payload:`, JSON.stringify(payload, null, 4))
    if (!payload.user.id) {
      return next(new Error('Acces token is not valid.'));
    }
    const userFound: User | undefined = await userCrud.getUser(payload.user.id);
    if (!userFound) {
      return next(new Error('Acces token is not valid.'));
    }
    (req as any).user = userFound;
    return next();
  } catch (error) {
    console.error(`${logMsg} Error to validate JWT`, error);
    return res.status(HttpStatus.unauthorized).json({
      error: {
        message: 'Error to validate access token.',
        details: (error as any).message
      }
    });
  }
}

export {
  validateJWT
};

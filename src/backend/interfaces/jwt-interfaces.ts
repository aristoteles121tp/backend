import * as JWT from 'jsonwebtoken';

interface IJWTData {
  refreshToken: string;
  seessionTimeInMinutes: number;
  tokenRefreshExpiryInHours: number;
  algorithm: JWT.Algorithm;
}

export {
  IJWTData
};
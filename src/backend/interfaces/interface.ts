interface ILetterQualification {
  letter: string;
  value: number;
}

interface IWordQualification {
  isWinner: boolean;
  response: ILetterQualification[];
}

interface IExededTRiesOrWin {
  exceededTries: boolean;
  wasWon: boolean;
}

export {
  ILetterQualification,
  IWordQualification,
  IExededTRiesOrWin
};